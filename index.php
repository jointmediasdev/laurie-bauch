<?php get_header(); ?>
			
	<div class="clearfix page-container home-page full-height">
        <div class="home-container mainSlider">

            <?php get_template_part( 'parts/loop', 'archive' ); ?>
            <?php // if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php // endwhile; endif; ?>
        
        </div>
    </div>

<?php get_footer(); ?>