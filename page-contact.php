<?php get_header(); ?>

            <div class="clearfix page-container contact-page full-height">
                <div class="row contact-container vert-center-rel">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                                
                            <div class="large-12 columns">
                                <?php the_content(); ?>
                                <div class="info">
                                    <span><i class="fa fa-envelope-o"></i> <a href="mailto:info@kaydesignandconsulting.com">info@kaydesignandconsulting.com</a></span><br>
                                    <span><i class="fa fa-phone"></i> <a href="tel:123.456.5678">123.456.5678</a></span>
                                </div>
                            </div>
                            <div class="large-11 large-offset-1 columns">
                                <?php gravity_form( 1, false, false, false, '', false ); ?>
                            </div>
                        
                    <?php endwhile; endif; ?>
                    
                </div>
            </div>
            <style>.contact-page { background-image: url(<?php the_field('background_image') ?>); }</style>

<?php get_footer(); ?>