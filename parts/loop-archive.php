<?php query_posts(array( 'post_type' => '', 'orderby' => 'rand' )); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div>
	<section class="large-8 columns">
		<article id="post-<?php the_ID(); ?>" <?php post_class('home-article'); ?> role="article" style="background-image: url('<?php the_field("post_image") ?>');">
			<div class="overlay">
				<div>
					<header class="article-header">
						<h2><?php the_title(); ?></h2>
					</header> <!-- end article header -->

					<section class="entry-content" itemprop="articleBody">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a>
						<?php the_content(); ?>
						<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="button tiny">Learn More</a>
					</section> <!-- end article section -->
				</div>
			</div>
		</article> <!-- end article -->
	</section>
	</div>
<?php endwhile; ?>	
					
<?php joints_page_navi(); ?>

<?php else : ?>
	<?php get_template_part( 'parts/content', 'missing' ); ?>
<?php endif; ?>
