<?php get_header(); ?>
			
           <div class="clearfix page-container general-template full-height">

                <div class="left-side">
                    
                    <?php if (get_field('general_image')): ?>
                        <style>.general-template .left-side { background-image: url('<?php the_field("general_image") ?>'); }</style>
                        <img src="<?php the_field('general_image') ?>" alt="Image">    

                    <?php endif ?>

                </div>
                

                <div class="right-side">

                    <div class="content">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    
                            <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
                                                    
                                <header class="article-header">
                                    <h2 class="text-center"><?php the_title(); ?></h2>
                                </header> <!-- end article header -->
                                                
                                <section class="entry-content" itemprop="articleBody">
                                    <?php the_content(); ?>
                                </section> <!-- end article section -->
                                                        
                                <?php //comments_template(); ?>
                                                
                            </article> <!-- end article -->
                            
                        <?php endwhile; endif; ?>

                    </div>

                </div>

            </div>

<?php get_footer(); ?>