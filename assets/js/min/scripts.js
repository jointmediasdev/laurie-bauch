// Init Foundation
jQuery(document).foundation();


/*********************
Functions
*********************/

    // Set Full Height Function
    function setFullHeight(obj, selector) {
        var total     = $(window).height(),
            top     = $(".header").height(),
            bottom = $(".bottom-line").outerHeight(true);

        if ($(window).width() > 960) {
            $(obj).css("height", "auto");
            $(obj).css("min-height", "auto");
            $(obj).css(selector, total - (top + bottom));
        }
        else {
            $(obj).css(selector, "auto");
        }
        
    }

    function heightForInspiration(obj) {
        var total     = $(window).height(),
            top     = $(".header").height(),
            bottom = $(".bottom-line").outerHeight(true),
            insirationPageHeight = $(obj).height() + (top + bottom);

        if (total > insirationPageHeight && $(window).width() > 960) {
            $(obj).find('.inspiration-container').addClass('vert-center-rel');
            $(obj).css("min-height", "auto");
            $(obj).css("height", total - (top + bottom));
        } else {
            $(obj).find('.inspiration-container').removeClass('vert-center-rel');
            $(obj).css("height", "auto");
            $(obj).css("min-height", total - (top + bottom));
        }

    }
    $('.mainSlider').slick({
        infinite: true,
        slidesToShow: 3,
        prevArrow: '<i class="fa fa-chevron-left prevArrow"></i>',
        nextArrow: '<i class="fa fa-chevron-right nextArrow"></i>',
        slidesToScroll: 1
    });

/*********************
Ready and Resize
*********************/
    $(document).ready(function() {

        setFullHeight(".page-container.full-height", "min-height");
        setFullHeight(".contact-page.full-height", "height");
        setFullHeight(".general-template.full-height", "height");
        setFullHeight(".home-page.full-height", "height");
        heightForInspiration(".inspiration-page");

        $(".page-container").addClass('loaded')

    });

    $(window).resize(function() {
        
        setFullHeight(".page-container.full-height", "min-height");
        setFullHeight(".contact-page.full-height", "height");
        setFullHeight(".general-template.full-height", "height");
        setFullHeight(".home-page.full-height", "height");
        heightForInspiration(".inspiration-page");
        
    });