<?php
/* joints Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the custom type
function clients_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'client_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Clients', 'jointstheme'), /* This is the Title of the Group */
			'singular_name' => __('Client', 'jointstheme'), /* This is the individual type */
			'all_items' => __('All Clients', 'jointstheme'), /* the all items menu item */
			'add_new' => __('Add Client', 'jointstheme'), /* The add new menu item */
			'add_new_item' => __('Add New Client', 'jointstheme'), /* Add New Display Title */
			'edit' => __( 'Edit', 'jointstheme' ), /* Edit Dialog */
			'edit_item' => __('Edit Client', 'jointstheme'), /* Edit Display Title */
			'new_item' => __('New Client', 'jointstheme'), /* New Display Title */
			'view_item' => __('View Client', 'jointstheme'), /* View Display Title */
			'search_items' => __('Search Clients', 'jointstheme'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'jointstheme'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'jointstheme'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Clients', 'jointstheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/assets/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'clients', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'clients', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	// register_taxonomy_for_object_type('category', 'client_type');
	/* this adds your post tags to your custom post type */
	// register_taxonomy_for_object_type('post_tag', 'client_type');
	
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'clients_post_type');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
    register_taxonomy( 'client_cat', 
    	array('client_type'), /* if you change the name of register_post_type( 'client_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
    		'labels' => array(
    			'name' => __( 'Client Categories', 'jointstheme' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Client Category', 'jointstheme' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Client Categories', 'jointstheme' ), /* search title for taxomony */
    			'all_items' => __( 'All Client Categories', 'jointstheme' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Client Category', 'jointstheme' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Client Category:', 'jointstheme' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Client Category', 'jointstheme' ), /* edit Client taxonomy title */
    			'update_item' => __( 'Update Client Category', 'jointstheme' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Client Category', 'jointstheme' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Client Category Name', 'jointstheme' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true, 
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'custom-slug' ),
    	)
    );   
    
	// now let's add custom tags (these act like categories)
    register_taxonomy( 'client_tag', 
    	array('client_type'), /* if you change the name of register_post_type( 'client_type', then you have to change this */
    	array('hierarchical' => false,    /* if this is false, it acts like tags */                
    		'labels' => array(
    			'name' => __( 'Client Tags', 'jointstheme' ), /* name of the Client taxonomy */
    			'singular_name' => __( 'Client Tag', 'jointstheme' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Client Tags', 'jointstheme' ), /* search title for taxomony */
    			'all_items' => __( 'All Client Tags', 'jointstheme' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Client Tag', 'jointstheme' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Client Tag:', 'jointstheme' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Client Tag', 'jointstheme' ), /* edit Client taxonomy title */
    			'update_item' => __( 'Update Client Tag', 'jointstheme' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Client Tag', 'jointstheme' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Client Tag Name', 'jointstheme' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    	)
    );



// let's create the function for the custom type
function references_post_type() { 
    // creating (registering) the custom type 
    register_post_type( 'reference_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
        // let's now add all the options for this post type
        array('labels' => array(
            'name' => __('References', 'jointstheme'), /* This is the Title of the Group */
            'singular_name' => __('Reference', 'jointstheme'), /* This is the individual type */
            'all_items' => __('All References', 'jointstheme'), /* the all items menu item */
            'add_new' => __('Add Reference', 'jointstheme'), /* The add new menu item */
            'add_new_item' => __('Add New Reference', 'jointstheme'), /* Add New Display Title */
            'edit' => __( 'Edit', 'jointstheme' ), /* Edit Dialog */
            'edit_item' => __('Edit Reference', 'jointstheme'), /* Edit Display Title */
            'new_item' => __('New Reference', 'jointstheme'), /* New Display Title */
            'view_item' => __('View Reference', 'jointstheme'), /* View Display Title */
            'search_items' => __('Search References', 'jointstheme'), /* Search Custom Type Title */ 
            'not_found' =>  __('Nothing found in the Database.', 'jointstheme'), /* This displays if there are no entries yet */ 
            'not_found_in_trash' => __('Nothing found in Trash', 'jointstheme'), /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
            ), /* end of arrays */
            'description' => __( 'References', 'jointstheme' ), /* Custom Type Description */
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
            'menu_icon' => get_stylesheet_directory_uri() . '/assets/images/custom-post-icon.png', /* the icon for the custom post type menu */
            'rewrite'   => array( 'slug' => 'references', 'with_front' => false ), /* you can specify its url slug */
            'has_archive' => 'references', /* you can rename the slug here */
            'capability_type' => 'post',
            'hierarchical' => false,
            /* the next one is important, it tells what's enabled in the post editor */
            'supports' => array( 'title', 'editor', 'revisions', 'sticky')
        ) /* end of options */
    ); /* end of register post type */
    
    /* this adds your post categories to your custom post type */
    // register_taxonomy_for_object_type('category', 'client_type');
    /* this adds your post tags to your custom post type */
    // register_taxonomy_for_object_type('post_tag', 'client_type');
    
} 

    // adding the function to the Wordpress init
    add_action( 'init', 'references_post_type');
    
    /*
    for more information on taxonomies, go here:
    http://codex.wordpress.org/Function_Reference/register_taxonomy
    */
    
    // now let's add custom categories (these act like categories)
    register_taxonomy( 'reference_cat', 
        array('reference_type'), /* if you change the name of register_post_type( 'client_type', then you have to change this */
        array('hierarchical' => true,     /* if this is true, it acts like categories */             
            'labels' => array(
                'name' => __( 'Reference Categories', 'jointstheme' ), /* name of the custom taxonomy */
                'singular_name' => __( 'Reference Category', 'jointstheme' ), /* single taxonomy name */
                'search_items' =>  __( 'Search Reference Categories', 'jointstheme' ), /* search title for taxomony */
                'all_items' => __( 'All Reference Categories', 'jointstheme' ), /* all title for taxonomies */
                'parent_item' => __( 'Parent Reference Category', 'jointstheme' ), /* parent title for taxonomy */
                'parent_item_colon' => __( 'Parent Reference Category:', 'jointstheme' ), /* parent taxonomy title */
                'edit_item' => __( 'Edit Reference Category', 'jointstheme' ), /* edit Reference taxonomy title */
                'update_item' => __( 'Update Reference Category', 'jointstheme' ), /* update title for taxonomy */
                'add_new_item' => __( 'Add New Reference Category', 'jointstheme' ), /* add new title for taxonomy */
                'new_item_name' => __( 'New Reference Category Name', 'jointstheme' ) /* name title for taxonomy */
            ),
            'show_admin_column' => true, 
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'custom-slug' ),
        )
    );   
    
    // now let's add custom tags (these act like categories)
    register_taxonomy( 'reference_tag', 
        array('reference_type'), /* if you change the name of register_post_type( 'client_type', then you have to change this */
        array('hierarchical' => false,    /* if this is false, it acts like tags */                
            'labels' => array(
                'name' => __( 'Reference Tags', 'jointstheme' ), /* name of the Reference taxonomy */
                'singular_name' => __( 'Reference Tag', 'jointstheme' ), /* single taxonomy name */
                'search_items' =>  __( 'Search Reference Tags', 'jointstheme' ), /* search title for taxomony */
                'all_items' => __( 'All Reference Tags', 'jointstheme' ), /* all title for taxonomies */
                'parent_item' => __( 'Parent Reference Tag', 'jointstheme' ), /* parent title for taxonomy */
                'parent_item_colon' => __( 'Parent Reference Tag:', 'jointstheme' ), /* parent taxonomy title */
                'edit_item' => __( 'Edit Reference Tag', 'jointstheme' ), /* edit Reference taxonomy title */
                'update_item' => __( 'Update Reference Tag', 'jointstheme' ), /* update title for taxonomy */
                'add_new_item' => __( 'Add New Reference Tag', 'jointstheme' ), /* add new title for taxonomy */
                'new_item_name' => __( 'New Reference Tag Name', 'jointstheme' ) /* name title for taxonomy */
            ),
            'show_admin_column' => true,
            'show_ui' => true,
            'query_var' => true,
        )
    ); 

?>