<?php get_header(); ?>
			<div class="clearfix page-container reference-archive full-height">
				
				<div class="row">
					<?php $postCount = 0; ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
						<?php // Incrementing postCount ?>
						<?php $postCount++; ?>
						<?php // Checking if last post or not ?>
              			<?php $hr =  ($postCount != sizeof($posts)) ? true : false  ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('large-18 small-centered columns end reference'); ?> role="article">					
							<section class="entry-content" itemprop="articleBody">
								<img class="reference-quote" src="<?php bloginfo( 'template_directory' ); ?>/assets/images/quote.svg" alt="" width="18" height="20">
								<em><?php the_content(); ?></em>
							</section> <!-- end article section -->

							<header class="article-header">
								<h2><span>{</span> <?php the_title(); ?> . <em><?php (get_field('persons_position') ? the_field('persons_position') : ''); ?></em> <span>}</span></h2>
							</header> <!-- end article header -->

						</article> <!-- end article -->
						
						<?php // Placing seperator if it's not the last post. ?>
						<?php if ($hr == true) { ?>
							<div class="large-10 small-centered column reference-hr"></div>
			            <?php } ?>
					    
						

					<?php endwhile; ?>	
										
					<?php joints_page_navi(); ?>

					<?php else : ?>
						<?php get_template_part( 'parts/content', 'missing' ); ?>
					<?php endif; ?>
									
			    </div> <!-- end .row -->

			</div> <!-- end .page-container -->

<?php get_footer(); ?>