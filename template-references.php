<?php
/*
Template Name: References Page Template
*/
?>

<?php get_header(); ?>

    <div class="clearfix page-container references-template full-height">

        <div class="content">

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
                                        
                    <section class="entry-content" itemprop="articleBody">
                        <?php the_content(); ?>
                    </section> <!-- end article section -->


                    <header class="article-header">
                        <h2 class="text-center"><?php the_title(); ?></h2>
                    </header> <!-- end article header -->
                                            
                    <?php //comments_template(); ?>
                                    
                </article> <!-- end article -->
                
            <?php endwhile; endif; ?>

        </div>

    </div>

<?php get_footer(); ?>