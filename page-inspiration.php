<?php get_header(); ?>

            <div class="clearfix page-container inspiration-page">
                <div class="row inspiration-container vert-center-rel">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                                
                        <div class="slider">
                            <img class="inspiration-image"src="<?php bloginfo( 'template_directory' ); ?>/assets/images/inspiration.jpg" alt="Inspiration">
                            <p class="content">Being good at business is the most<br>fascinating kind of art.</p>
                            <img class="inspiration-signature"src="<?php bloginfo( 'template_directory' ); ?>/assets/images/signature.jpg" alt="Inspiration">
                        </div>
                        
                    <?php endwhile; endif; ?>
                    
                </div>
            </div>

<?php get_footer(); ?>