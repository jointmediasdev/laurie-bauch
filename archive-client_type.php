<?php get_header(); ?>
			<div class="clearfix page-container client-archive full-height">

					<div class="row vert-center-abs">

				

					<?php if (have_posts()) : $count = 0; while (have_posts()) : the_post(); $count++; ?>
						<?php //echo $count; ?>
						<?php
						$count_posts = wp_count_posts();

						$published_posts = $count_posts->publish;
						?>
						<?php echo $published_posts; ?>
	
						<article id="post-<?php the_ID(); ?>" <?php post_class('large-8 medium-12 columns end client item' . $count); ?> role="article">		

							<header class="article-header">
								<h2 class="ir"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
								<img class="vert-center-rel" src="<?php the_field('logo') ?>" alt="">
							</header> <!-- end article header -->
											
							<section class="entry-content" itemprop="articleBody">
								<?php if (has_post_thumbnail()): ?>
									<?php the_post_thumbnail( 'clients-thumb' ) ?>
								<?php else: ?>
									<img src="http://placehold.it/443x292" alt="">
								<?php endif ?>
								
							</section> <!-- end article section -->

						</article> <!-- end article -->

					<?php endwhile; ?>	
										
					<?php joints_page_navi(); ?>

					<?php else : ?>
						<?php get_template_part( 'parts/content', 'missing' ); ?>
					<?php endif; ?>
									
			    </div> <!-- end .row -->

			</div> <!-- end .page-container -->

<?php get_footer(); ?>